pub fn rust_release_mode() -> bool {
    cfg!(not(debug_assertions))
}

pub fn environment_info() -> String {
    let env = format!(
        "OS: {}\nFamily: {}\nCPU: {}\nDebug: {}",
        std::env::consts::OS,
        std::env::consts::FAMILY,
        std::env::consts::ARCH,
        (!rust_release_mode()).to_string()
    );
    log::info!("rust environment_info = {}", env);
    return env;
}

// http://cjycode.com/flutter_rust_bridge/feature/stream.html
// https://github.com/fzyzcjy/flutter_rust_bridge/issues/347
// https://gist.github.com/Desdaemon/be5da0a1c6b4724f20093ef434959744
use anyhow::Result;
use std::{
    sync::atomic::{AtomicUsize, Ordering},
    thread::sleep,
    time::Duration,
};

use flutter_rust_bridge::StreamSink;

use crate::logger;

const ONE_SECOND: Duration = Duration::from_secs(1);

static STOPPED: AtomicUsize = AtomicUsize::new(0);

// can't omit the return type yet, this is a bug
pub fn tick(sink: StreamSink<u16>) -> Result<()> {
    let id = STOPPED.fetch_add(1, Ordering::Relaxed) + 1;
    let mut ticks = 0;
    while STOPPED.load(Ordering::Relaxed) <= id {
        sink.add(ticks);
        log::debug!("sunk tick: {}", ticks);
        sleep(ONE_SECOND);
        ticks = ticks.wrapping_add(1);
    }
    log::info!("ticker #{id} stopped");
    Ok(())
}

pub fn stop_ticking() {
    STOPPED.fetch_add(1, Ordering::Relaxed);
}

// https://github.com/fzyzcjy/flutter_rust_bridge/issues/486
pub struct LogEntry {
    pub time_millis: i64,
    pub level: i32,
    pub tag: String,
    pub msg: String,
}

impl LogEntry {
    pub const LEVEL_TRACE: i32 = 5000;
    pub const LEVEL_DEBUG: i32 = 10000;
    pub const LEVEL_INFO: i32 = 20000;
    pub const LEVEL_WARN: i32 = 30000;
    pub const LEVEL_ERROR: i32 = 40000;
}

pub fn create_log_stream(s: StreamSink<LogEntry>) -> Result<()> {
    logger::SendToDartLogger::set_stream_sink(s);
    Ok(())
}

pub fn rust_set_up() {
    logger::init_logger();
}
