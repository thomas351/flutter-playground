# flutter_playground

A playground I use to try Flutter and related projects of interest to me.

# Release 

- Android (17.5MB)

        flutter build apk --release
        adb install build/app/outputs/flutter-apk/app-release.apk

- Linux desktop (19.6MB generated, after removing unused assets 18.7MB):

        flutter build linux --release
        rm -R build/linux/x64/release/bundle/data/flutter_assets
        build/linux/x64/release/bundle/flutter_playground

- [Web](https://docs.flutter.dev/development/tools/web-renderers) [for publishing in a flutter_playground subdirectory of your webserver] (2.4MB):

        flutter build web --web-renderer html --base-href /flutter_playground/ --release

# History

1. Generate default flutter sample:

        flutter create flutter_playground

2. Init git repository and publish it to Gitlab:

        cd flutter_playground
        git init --initial-branch=main
        git remote add origin https://gitlab.com/thomas351/flutter-playground.git
        git config user.name "thomas351"
        git config --global user.email "******@*****.**"
        git add .
        git commit -m "default flutter sample, only changed README.md and initialized git"
        git push -u origin main

3. Open it up in VS Code and see if the sample works on all available plattforms (which for me are android, chrome & linux desktop)

        code .

4. Save ourself 2MB in the web version by getting rid of the fonts: Set `uses-material-design: false` and removing the `cupertino_icons` dependency in `pubspec.yaml`.

5. Honor device theme settings: set light & dark theme to MaterialApp Widget.

6. Extract & customize light and dark themes.

7. add workaround for blurry text in some browsers, see: https://github.com/flutter/flutter/issues/81215

8. extract homepage screen into a separate file

9. use easy_dynamic_theme plugin for interactive theme switching. Add a Settings screen with a button to switch the theme. Make Settings screen reachable through a '⁝' action button on the AppBar of the homescreen.

10. add settings_ui plugin with sample, for a nicer settings page.

11. Workaround for theme switching of settings_ui, see https://github.com/yako-dev/flutter-settings-ui/issues/116

12. Make scrollbar visible for darkTheme on settings screen.

13. Add logger plugin and use it in getSettingsTheme function.

14. Add compact log printer, modified version of https://medium.com/flutter-community/a-guide-to-setting-up-better-logging-in-flutter-3db8bab2000e

(for more look at the commit history, I'm trying to keep it as clean as possible)

## Flutter Rust Bridge Setup

To begin, ensure that you have a working installation of the following items:
- [Flutter SDK](https://docs.flutter.dev/get-started/install)
- [Rust language](https://rustup.rs/)
- Appropriate [Rust targets](https://rust-lang.github.io/rustup/cross-compilation.html) for cross-compiling to your device
- For Android targets:
    - Install [cargo-ndk](https://github.com/bbqsrc/cargo-ndk#installing)
    - Install Android NDK 22, then put its path in one of the `gradle.properties`, e.g.:

```
echo "ANDROID_NDK=.." >> ~/.gradle/gradle.properties
```

- Web is not supported yet.

Then go ahead and run `flutter run`! When you're ready, refer to our documentation
[here](https://fzyzcjy.github.io/flutter_rust_bridge/index.html)
to learn how to write and use binding code.

# Run / Debug

- flutter run -d android --release
- flutter run -d linux --release
- flutter run -d linux --profile
- flutter run -d linux --debug

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
