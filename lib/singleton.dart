import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'compact_logger.dart';
import 'ffi.dart';

class MySingleton {
  static final MySingleton _instance = MySingleton._internal();
  var logger = getLogger("Singleton");
  var format = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');

  factory MySingleton() {
    return _instance;
  }

  MySingleton._internal() {
    if (kDebugMode) {
      logger.d("MySingleton initialized");
    }
    api.rustSetUp();
    api.createLogStream().listen((event) {
      var print =
          'RustLogger: [${event.tag}] ${event.msg} (sent ${format.format(DateTime.fromMillisecondsSinceEpoch(event.timeMillis))})';

      if (event.level <= LogEntry_LEVEL_DEBUG) {
        logger.d(print);
      } else if (event.level <= LogEntry_LEVEL_INFO) {
        logger.i(print);
      } else if (event.level <= LogEntry_LEVEL_WARN) {
        logger.w(print);
      } else if (event.level <= LogEntry_LEVEL_ERROR) {
        logger.e(print);
      } else {
        logger.wtf(print);
      }

      // Log.instance.log(event.level, _kRustTagPrefix + event.tag, '${event.msg}(rust_time=${event.timeMillis})');
    });
  }
}
