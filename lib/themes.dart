import 'dart:ui';

import 'package:easy_dynamic_theme/easy_dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

import 'compact_logger.dart';

var logger = getLogger("themes");

var lightThemeData = ThemeData.light().copyWith(
  colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.green).copyWith(
    secondary: Colors.red,
    brightness: Brightness.light,
  ),
  textTheme: ThemeData.light().textTheme.copyWith(
        // to make text also crisp in web browser mode, see: https://github.com/flutter/flutter/issues/81215
        displayLarge: ThemeData.light().textTheme.displayLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        displayMedium: ThemeData.light().textTheme.displayMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        displaySmall: ThemeData.light().textTheme.displaySmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        headlineLarge: ThemeData.light().textTheme.headlineLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        headlineMedium: ThemeData.light().textTheme.headlineMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        headlineSmall: ThemeData.light().textTheme.headlineSmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        titleLarge: ThemeData.light().textTheme.titleLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        titleMedium: ThemeData.light().textTheme.titleMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        titleSmall: ThemeData.light().textTheme.titleSmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        bodyLarge: ThemeData.light().textTheme.bodyLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        bodyMedium: ThemeData.light().textTheme.bodyMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        bodySmall: ThemeData.light().textTheme.bodySmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
      ),
);

var darkThemeData = ThemeData.dark().copyWith(
  colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.green).copyWith(
    primary: const Color.fromARGB(255, 11, 49, 13),
    secondary: const Color.fromARGB(255, 80, 11, 7),
  ),
  scrollbarTheme: const ScrollbarThemeData().copyWith(
    thumbColor: MaterialStateProperty.resolveWith((states) {
      if (states.intersection({
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
        MaterialState.dragged,
        MaterialState.scrolledUnder,
      }).isNotEmpty) {
        return const Color.fromARGB(90, 255, 255, 255);
      }
      return const Color.fromARGB(20, 255, 255, 255);
    }),
    //.all(),
  ),
  scaffoldBackgroundColor: Colors.black,
  textTheme: ThemeData.dark().textTheme.copyWith(
        // to make text also crisp in web browser mode, see: https://github.com/flutter/flutter/issues/81215
        displayLarge: ThemeData.dark().textTheme.displayLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        displayMedium: ThemeData.dark().textTheme.displayMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        displaySmall: ThemeData.dark().textTheme.displaySmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        headlineLarge: ThemeData.dark().textTheme.headlineLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        headlineMedium: ThemeData.dark().textTheme.headlineMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        headlineSmall: ThemeData.dark().textTheme.headlineSmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        titleLarge: ThemeData.dark().textTheme.titleLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        titleMedium: ThemeData.dark().textTheme.titleMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        titleSmall: ThemeData.dark().textTheme.titleSmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        bodyLarge: ThemeData.dark().textTheme.bodyLarge?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        bodyMedium: ThemeData.dark().textTheme.bodyMedium?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
        bodySmall: ThemeData.dark().textTheme.bodySmall?.copyWith(
          fontFeatures: [const FontFeature.proportionalFigures()],
        ),
      ),
);

var lightSettingsTheme = const SettingsThemeData(
    // settingsListBackground: Color.fromARGB(255, 204, 198, 141),
    // dividerColor: Colors.black,
    // inactiveTitleColor: Color.fromARGB(255, 75, 75, 75),
    // inactiveSubtitleColor: Color.fromARGB(255, 75, 75, 75),
    );

var darkSettingsTheme = const SettingsThemeData(
  settingsListBackground: Colors.black,
  settingsSectionBackground: Color.fromARGB(200, 15, 15, 15),
  dividerColor: Colors.green,
  inactiveTitleColor: Colors.grey,
  inactiveSubtitleColor: Colors.grey,
  settingsTileTextColor: Colors.white,
  titleTextColor: Color.fromARGB(255, 75, 174, 255),
);

SettingsThemeData getSettingsTheme(BuildContext context) {
  switch (EasyDynamicTheme.of(context).themeMode) {
    case ThemeMode.dark:
      logger.d(
          "EasyDynamicTheme.of(context).themeMode == ThemeMode.dark -> using darkTheme");
      return darkSettingsTheme;
    case ThemeMode.light:
      logger.d(
          "EasyDynamicTheme.of(context).themeMode == ThemeMode.light -> using lightTheme");
      return lightSettingsTheme;
    case ThemeMode.system:
      switch (MediaQuery.platformBrightnessOf(context)) {
        case Brightness.light:
          logger.d("EasyDynamicTheme.of(context).themeMode == ThemeMode.system "
              "&& MediaQuery.platformBrightnessOf(context) == Brightness.light -> using lightTheme");
          return lightSettingsTheme;
        case Brightness.dark:
          logger.d("EasyDynamicTheme.of(context).themeMode == ThemeMode.system "
              "&& MediaQuery.platformBrightnessOf(context) == Brightness.dark -> using darkTheme");
          return darkSettingsTheme;
        default:
          logger.w(
              "got unknown platform Brightness from MediaQuery.platformBrightness: " +
                  MediaQuery.platformBrightnessOf(context).toString() +
                  " (using darkTheme)");
      }
      break;
    default:
      logger.w("got unkown ThemeMode from EasyDynamicTheme: " +
          EasyDynamicTheme.of(context).themeMode.toString() +
          " (using darkTheme)");
  }
  return darkSettingsTheme;
}

String getCurrentThemeModeString(BuildContext context) {
  return EasyDynamicTheme.of(context)
      .themeMode
      .toString()
      .replaceFirst("ThemeMode.", "");
}

String getThemeSymbol(BuildContext context) {
  switch (EasyDynamicTheme.of(context).themeMode) {
    case ThemeMode.system:
      return "☽";
    case ThemeMode.light:
      return "☀";
    case ThemeMode.dark:
      return "☼";
    default:
      return "?";
  }
}
