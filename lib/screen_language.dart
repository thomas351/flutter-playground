import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
// alternative fork of settings_ui, a bit older:
// import 'package:flutter_settings_ui/flutter_settings_ui.dart';

import 'themes.dart';

class LanguagesScreen extends StatefulWidget {
  const LanguagesScreen({Key? key}) : super(key: key);

  @override
  _LanguagesScreenState createState() => _LanguagesScreenState();
}

class _LanguagesScreenState extends State<LanguagesScreen> {
  int languageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Languages'),
        leading: Tooltip(
          child: ElevatedButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('<',
                style: Theme.of(context)
                    .textTheme
                    .headline4), // or one of those symbols? -> ← ↩
            style: ElevatedButton.styleFrom(
                elevation: 0 // , primary: Colors.transparent
                ),
          ),
          message: "back",
        ),
      ),
      body: SettingsList(
        lightTheme: getSettingsTheme(context),
        sections: [
          SettingsSection(tiles: [
            SettingsTile(
              title: const Text("English"),
              trailing: trailingWidget(0),
              onPressed: (BuildContext context) {
                changeLanguage(0);
              },
            ),
            SettingsTile(
              title: const Text("Spanish"),
              trailing: trailingWidget(1),
              onPressed: (BuildContext context) {
                changeLanguage(1);
              },
            ),
            SettingsTile(
              title: const Text("Chinese"),
              trailing: trailingWidget(2),
              onPressed: (BuildContext context) {
                changeLanguage(2);
              },
            ),
            SettingsTile(
              title: const Text("German"),
              trailing: trailingWidget(3),
              onPressed: (BuildContext context) {
                changeLanguage(3);
              },
            ),
          ]),
        ],
      ),
    );
  }

  Widget trailingWidget(int index) {
    return (languageIndex == index)
        ? const Icon(Icons.check, color: Colors.blue)
        : const Icon(null);
  }

  void changeLanguage(int index) {
    setState(() {
      languageIndex = index;
    });
  }
}
