import 'package:intl/intl.dart';
import 'package:logger/logger.dart';

class CompactPrinter extends LogPrinter {
  var format = DateFormat('yyyy-MM-dd HH:mm:ss.SSS');
  final String className;
  CompactPrinter(this.className);

  @override
  List<String> log(LogEvent event) {
    var color = PrettyPrinter.levelColors[event.level];
    var emoji = PrettyPrinter.levelEmojis[event.level];
    var message = event.message;
    return [
      color!('${format.format(DateTime.now())} $emoji$className: $message')
    ];
  }
}

Logger getLogger(String className) {
  return Logger(printer: CompactPrinter(className), level: Level.verbose);
}
