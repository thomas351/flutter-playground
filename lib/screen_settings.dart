import 'package:easy_dynamic_theme/easy_dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
// alternative fork of settings_ui, a bit older:
// import 'package:flutter_settings_ui/flutter_settings_ui.dart';

import 'screen_language.dart';
import 'themes.dart';

import 'ffi.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool lockInBackground = true;
  bool notificationsEnabled = true;
  late Future<String> environmentInfo;

  @override
  void initState() {
    super.initState();
    environmentInfo = api.environmentInfo();
  }

  @override
  void dispose() {
    api.stopTicking();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
        leading: Tooltip(
          child: ElevatedButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text('<',
                style: Theme.of(context)
                    .textTheme
                    .headline4), // or one of those symbols? -> ← ↩
            style: ElevatedButton.styleFrom(
                elevation: 0 // , primary: Colors.transparent
                ),
          ),
          message: "back",
        ),
      ),
      body: buildSettingsList(context),
    );
  }

  Widget buildSettingsList(BuildContext context) {
    return SettingsList(
      lightTheme: getSettingsTheme(context),
      sections: [
        SettingsSection(
          title: const Text('Common'),
          tiles: [
            SettingsTile(
              title: const Text('Theme'),
              value: Text(getCurrentThemeModeString(context)),
              leading: Text(getThemeSymbol(context)),
              onPressed: (context) {
                EasyDynamicTheme.of(context).changeTheme();
              },
            ),
            // SettingsTile(
            //   title: const Text('Log'),
            //   description: const Text('click to open'),
            //   leading: const Icon(Icons.logo_dev),
            //   onPressed: (context) {
            //     LogConsole.open(context);
            //   },
            // ),
            SettingsTile(
              title: const Text('Language'),
              description: const Text('English'),
              leading: const Icon(Icons.language),
              onPressed: (context) {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => const LanguagesScreen(),
                ));
              },
            ),
            CustomSettingsTile(
              child: Container(
                // color: const Color(0xFFEFEFF4),
                padding: const EdgeInsetsDirectional.only(
                  start: 14,
                  top: 12,
                  bottom: 30,
                  end: 14,
                ),
                child: const Text(
                  'You can setup the language you want',
                  style: TextStyle(
                    // color: Colors.grey.shade700,
                    fontWeight: FontWeight.w400,
                    fontSize: 13.5,
                    letterSpacing: -0.5,
                  ),
                ),
              ),
            ),
            SettingsTile(
              title: const Text('Environment'),
              description: FutureBuilder<String>(
                  future: environmentInfo,
                  builder: (context, snap) {
                    if (snap.error != null) {
                      // An error has been encountered, so give an appropriate response and
                      // pass the error details to an unobstructive tooltip.
                      debugPrint(snap.error.toString());
                      return Tooltip(
                        message: snap.error.toString(),
                        child: const Text('Unknown Runtime'),
                      );
                    }

                    // Guard return here, the data is not ready yet.
                    final data = snap.data;
                    if (data == null) {
                      return const CircularProgressIndicator();
                    } else {
                      return Text(data);
                    }
                  }),
              leading: const Icon(Icons.cloud_queue),
            ),
            SettingsTile(
              title: const Text('Time since starting Rust stream'),
              description: StreamBuilder<int>(
                  stream: api.tick(),
                  builder: (context, snap) {
                    final style = Theme.of(context).textTheme.headline4;
                    final error = snap.error;
                    if (error != null) {
                      return Tooltip(
                          message: error.toString(),
                          child: Text('Error', style: style));
                    }
                    final data = snap.data;
                    if (data != null) {
                      if (data == 1) {
                        return Text('$data second', style: style);
                      } else {
                        return Text('$data seconds', style: style);
                      }
                    }
                    return const CircularProgressIndicator();
                  }),
              leading: const Icon(Icons.cloud_queue),
            ),
          ],
        ),
        SettingsSection(
          title: const Text('Account'),
          tiles: [
            SettingsTile(
                title: const Text('Phone number'),
                leading: const Icon(Icons.phone)),
            SettingsTile(
                title: const Text('Email'), leading: const Icon(Icons.email)),
            SettingsTile(
                title: const Text('Sign out'),
                leading: const Icon(Icons.exit_to_app)),
          ],
        ),
        SettingsSection(
          title: const Text('Security'),
          tiles: [
            SettingsTile.switchTile(
              title: const Text('Lock app in background'),
              leading: const Icon(Icons.phonelink_lock),
              initialValue: lockInBackground,
              onToggle: (bool value) {
                setState(() {
                  lockInBackground = value;
                  notificationsEnabled = value;
                });
              },
            ),
            SettingsTile.switchTile(
              title: const Text('Use fingerprint'),
              description: const Text(
                  'Allow application to access stored fingerprint IDs.'),
              leading: const Icon(Icons.fingerprint),
              onToggle: (bool value) {},
              initialValue: false,
            ),
            SettingsTile.switchTile(
              title: const Text('Change password'),
              leading: const Icon(Icons.lock),
              initialValue: true,
              onToggle: (bool value) {},
            ),
            SettingsTile.switchTile(
              title: const Text('Enable Notifications'),
              enabled: notificationsEnabled,
              leading: const Icon(Icons.notifications_active),
              initialValue: true,
              onToggle: (value) {},
            ),
          ],
        ),
        SettingsSection(
          title: const Text('Misc'),
          tiles: [
            SettingsTile(
                title: const Text('Terms of Service'),
                leading: const Icon(Icons.description)),
            SettingsTile(
                title: const Text('Open source licenses'),
                leading: const Icon(Icons.collections_bookmark)),
          ],
        ),
      ],
    );
  }
}
